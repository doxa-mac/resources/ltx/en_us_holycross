
# ltx en_us_holycross 
Liturgical translation files used by Doxa when generating a liturgical website.

The files are © Liturgical Texts courtesy and copyright of Holy Cross Press, Brookline, MA

The files in this repository are formatted for use with Doxa.

Use of these files is subject to the terms stated in the LICENSE file.

[Doxa](https://doxa.liml.org) is a liturgical software product from the [Orthodox Christian Mission Center](https://ocmc.org). 

[Doxa install link](https://github.com/liturgiko/doxa/releases)
